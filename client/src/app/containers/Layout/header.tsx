import React from 'react';
import { Helmet } from 'react-helmet-async';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import logo from '../../assets/images/aizove-logo.png';
export function Header() {
  return (
    <>
      <div>
        <nav className="navbar pt-0 pb-0 aizove-nav font-avg navbar-expand-lg navbar-light bg-white shadow-sm">
          <div className="container-fluid">
            <Link className="navbar-brand mr-5" to="/home">
              <img src={logo} />
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarTogglerDemo01"
              aria-controls="navbarTogglerDemo01"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
              <div className="d-flex ml-auto">
                <ul className="navbar-nav font-normal fg-dk-gray mr-auto mb-2 mb-lg-0">
                  <li className="nav-item float-right">
                    <a className="nav-link" href="/home#research">
                      Research
                    </a>
                  </li>
                  <li className="nav-item float-right">
                    <a className="nav-link " href="/home#solutions">
                      Solutions
                    </a>
                  </li>
                  <li className="nav-item float-right">
                    <a className="nav-link " href="/home#solutions">
                      Contact Us
                    </a>
                  </li>

                  <li className="nav-item ml-4 float-right">
                    <Link className="fg-green font-avg btn" to="/auth/login">
                      Sign in
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </>
  );
}
