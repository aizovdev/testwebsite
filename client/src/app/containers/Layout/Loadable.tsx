/**
 * Asynchronously loads the component for Layout
 */

import { lazyLoad } from 'utils/loadable';

export const Header = lazyLoad(
  () => import('./header'),
  module => module.Header,
);
export const Footer = lazyLoad(
  () => import('./footer'),
  module => module.Footer,
);
