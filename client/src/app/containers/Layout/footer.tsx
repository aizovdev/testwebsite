import React from 'react';
import { FaLongArrowAltRight, FaLinkedinIn } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/aizove-logo.png';
export function Footer() {
  return (
    <>
      <div className="bg-gray pt-5 pb-3">
        <div className="container footer">
          <div className="row m-0">
            <div className="col-lg-3 col-12 p-0-xs">
              <a className="brand-logo p-0 col-12 d-block mb-4" href="#">
                <img src={logo} />
              </a>
              <p>
                Aizove is operating in the UAE, Saudi Arabia, France, India, and
                Belgium. As our work has been highly recognized, the company is
                rapidly growing across the globe, helping people by providing
                the right solution.
              </p>
              <h3 className="font-bold">Contact Us</h3>
              <p>Phone Number : +971 (0) 5 432 2079</p>
              <p>Email : info@aizov.com</p>
            </div>

            <div className="col-lg-3 col-12 mt-4 p-0-xs">
              <h3 className="font-bold mb-4">Solutions</h3>
              <p>
                <ul>
                  <li>Technology</li>
                  <li>Digital Transformation</li>
                  <li>Trade</li>
                  <li>Education</li>
                  <li>Transportation and logistics</li>
                  <li>Energy</li>
                  <li>Community</li>
                  <li>Biodiversity</li>
                  <li>Business automation</li>
                </ul>
              </p>
            </div>
            <div className="col-3 mt-4 p-0-xs">
              <h3 className="font-bold mb-4">Company</h3>
              <p>
                <ul>
                  <li>
                    <a className="fg-black" href="/home#research">
                      Research
                    </a>
                  </li>
                  <li>
                    <a className="fg-black" href="/home#solutions">
                      Solutions
                    </a>
                  </li>
                </ul>
              </p>
            </div>
            <div className="col-lg-3 col-12 mt-4 p-0-xs">
              <h3 className="font-bold mb-4">Newsletter</h3>
              <p>Get our latest updates to your email</p>
              <p>
                <div>
                  <div className="input-group mb-3">
                    <input
                      type="text"
                      className="form-control tall-box"
                      placeholder="Recipient's username"
                      aria-label="Recipient's username"
                      aria-describedby="basic-addon2"
                    />
                    <div className="input-group-append">
                      <button className="btn bg-yellow" type="button">
                        <FaLongArrowAltRight size="14" />
                      </button>
                    </div>
                  </div>
                </div>
              </p>

              <p>
                <div className="social">
                  <a href="#" target="_blank">
                    <FaLinkedinIn size="16" />
                  </a>
                </div>
              </p>
            </div>
          </div>
          <div className="bottom-footer">
            <p>© 2020 Copyright Aizove.All rights reserved</p>
          </div>
        </div>
      </div>
    </>
  );
}
