/**
 * Asynchronously loads the component for HomePage
 */

import { lazyLoad } from 'utils/loadable';

export const Login = lazyLoad(
  () => import('./login'),
  module => module.Login,
);

export const Signup = lazyLoad(
  () => import('./signup'),
  module => module.Signup,
);
export const Otpverification = lazyLoad(
  () => import('./otpverification'),
  module => module.Otpverification,
);
export default Login;
