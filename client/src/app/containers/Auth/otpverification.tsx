import React from 'react';
import Otp from '../../components/forms/otp';

export function Otpverification() {
  return (
    <>
      <div className="container p-5 text-center">
        <Otp />
      </div>
    </>
  );
}
