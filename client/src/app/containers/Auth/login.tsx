import React from 'react';
import { Helmet } from 'react-helmet-async';
import LoginForm from '../../components/forms/signin';
import logo from '../../assets/images/aizove-logo.png';

export function Login() {
  return (
    <>
      <Helmet>
        <title>Aizove Login</title>
        <meta
          name="description"
          content="Sign in and post your votes, Your vote matters to us"
        />
      </Helmet>
      {/* <span>HomePage container</span> */}
      <div className="container p-5-md py-5 text-center">
        <LoginForm />
      </div>
    </>
  );
}
