import React from 'react';
import { Helmet } from 'react-helmet-async';
import SignupForm from '../../components/forms/signup';
export function Signup() {
  return (
    <>
      <Helmet>
        <title>Aizove Signup</title>
        <meta
          name="description"
          content="Create your aizove account and be part of the revolution"
        />
      </Helmet>
      {/* <span>HomePage container</span> */}
      <div className="container p-5-md py-5 text-center">
        <SignupForm />
      </div>
    </>
  );
}
