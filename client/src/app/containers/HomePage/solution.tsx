import React, { Component } from 'react';
import { Link } from 'react-router-dom';
export default class solution extends Component {
  render() {
    return (
      <div className="home-top-banner pt-5">
        <div className="container">
          <div className="header">
            <h6 className="underline-head fg-blue">Our latest Solutions</h6>
            <h4 className="col-md-4 col-12 pt-4 p-0 font-title font-bold">
              We bring your ideas and work for it to become reality
            </h4>
          </div>
          <div className="content row mt-5 m-0">
            <div className="box-full h-100 col-12 px-0 mb-4">
              <img src="/images/news/trade.jpg" />
              <div className="hover"></div>
              <div className="contents p-5 fg-white">
                <span>Trade Automation Platform ( TAP )</span>
                <h4 className="mt-4 col-12 col-4 pl-0">
                  Breaking the obstacles of online and offline trade market
                </h4>
                <Link className="mt-4 fg-white col-12 p-0" to="#">
                  Read More
                </Link>
              </div>
            </div>
            <div className="box-full col-12 col-md-6 mb-4 p-0">
              <div className="col-12 h-100 position-relative pl-0">
                <div className="col-12 h-100 position-relative position-absolute-xs p-0">
                  <img src="/images/news/education.jpg" />

                  <div className="hover"></div>
                </div>
                <div className="contents p-5 fg-white">
                  <span>Coach parrot</span>
                  <h4 className="mt-4 col-12 col-4 pl-0">
                    Inspiring insights of career guidance
                  </h4>
                  <Link className="mt-4 fg-white col-12 p-0" to="#">
                    Read More
                  </Link>
                </div>
              </div>
            </div>
            <div className="box-full col-md-6 col-12 mb-4 p-0">
              <div className="col-12 h-100 position-relative p-0">
                <div className="col-12 p-0 h-100 position-relative position-absolute-xs p-0">
                  <img src="/images/news/biotech.jpg" />
                  <div className="hover"></div>
                </div>
                <div className="contents p-5 fg-white">
                  <span>Heva</span>
                  <h4 className="mt-4 col-12 col-4 pl-0">
                    Agritech: Presenting to you the most diverse of plants
                  </h4>
                  <Link className="mt-4 fg-white col-12 p-0" to="#">
                    Read More
                  </Link>
                </div>
              </div>
            </div>
            <div className="box-full col-12 px-0 mb-4">
              <div className="col-12 p-0 h-100 position-relative position-absolute-xs p-0">
                <img src="/images/news/corporate.jpg" />
                <div className="hover"></div>
              </div>
              <div className="contents p-5 fg-white">
                <span>Enterprice Automation Platform (EAP)</span>
                <h4 className="mt-4 col-12 col-4 pl-0">
                  Customizable business automation solution that integrates
                  favoring the needs of our clients.
                </h4>
                <Link className="mt-4 fg-white col-12 p-0" to="#">
                  Read More
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
