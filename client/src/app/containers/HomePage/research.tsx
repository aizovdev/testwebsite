import React, { Component } from 'react';
import { Link } from 'react-router-dom';
export default class Research extends Component {
  render() {
    return (
      <div className="home-top-banner pt-5">
        <div className="container">
          <div className="header">
            <h6 className="underline-head fg-blue">Our recent Case studies</h6>
            <h4 className="col-md-4 col-12 py-4 p-0 font-title font-bold">
              We work together for a healthier community
            </h4>
          </div>
          <div className="content m-0 row">
            <div className="box-half col-12 col-md-6 p-0">
              <div className="col-12 position-relative pl-0">
                <div className="image-container">
                  <div className="hover"></div>
                  <img src="/images/news/sustainable.jpg" />
                </div>
                <div className="contents py-5 fg-black position-relative">
                  <div
                    className="industry bg-green font-bold fg-white"
                    title="water"
                  >
                    S
                  </div>
                  <h4 className="mt-5 col-12 font-medium col-4 pl-0">
                    Sustainable development
                  </h4>
                  <p>
                    Overwhelmed by the disaster of human resources, caused what
                    is called the major decline of sustainability. The need for
                    sustainable development- moving towards a circular economy
                    is the key to healthy development
                  </p>
                  <Link className="mt-4 fg-white col-12 p-0" to="#">
                    Read More
                  </Link>
                </div>
              </div>
            </div>
            <div className="box-half col-12 col-md-6 p-0">
              <div className="col-12 position-relative pl-0">
                <div className="image-container">
                  <div className="hover"></div>
                  <img src="/images/news/aizo-robot.jpg" />
                </div>
                <div className="contents py-5 fg-black position-relative">
                  <div
                    className="industry bg-green font-bold fg-white"
                    title="water"
                  >
                    R
                  </div>
                  <h4 className="mt-5 col-12 font-medium col-4 pl-0">
                    AI powered robots to make an appearance in educational
                    spaces
                  </h4>
                  <p>
                    Aizo is a humanoid robot is designed and developed for
                    educational institutes and students. We are introducing and
                    improving the new era for the educational system. How it
                    will work?
                  </p>
                  <Link className="mt-4 fg-white col-12 p-0" to="#">
                    Read More
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="content row m-0">
            <div className="box-half col-12 col-md-6 p-0">
              <div className="col-12 h-100 position-relative pl-0">
                <div className="image-container">
                  <div className="hover"></div>
                  <img src="/images/news/store.jpg" />
                </div>
                <div className="contents py-5 fg-black position-relative">
                  <div
                    className="industry bg-purple font-bold fg-white"
                    title="Electricity"
                  >
                    T
                  </div>
                  <h4 className="mt-5 col-12 font-medium col-4 pl-0">
                    Competing with the recession of global trade
                  </h4>
                  <p>
                    Enabling localized and international digital twin platform
                    for people to access retail shops and products. We strive to
                    provide social services enhancing business ideas for growing
                    better platform for digital trading.
                  </p>
                  <Link className="mt-4 fg-white col-12 p-0" to="#">
                    Read More
                  </Link>
                </div>
              </div>
            </div>
            <div className="box-half col-12 col-md-6 p-0">
              <div className="col-12 h-100 position-relative pl-0">
                <div className="image-container">
                  <div className="hover"></div>
                  <img src="/images/news/aizor.jpg" />
                </div>
                <div className="contents py-5 fg-black position-relative">
                  <div
                    className="industry bg-green font-bold fg-white"
                    title="Water"
                  >
                    A
                  </div>
                  <h4 className="mt-5 col-12 font-medium col-4 pl-0">
                    Be the Aizor in Aizove
                  </h4>
                  <p>
                    Let this be your next ultimate solution for delivery issues.
                    A quick and easier way for people to get a delivery agents.
                  </p>
                  <Link className="mt-4 fg-white col-12 p-0" to="#">
                    Read More
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="content row m-0">
            <div className="box-half col-12 col-md-6 p-0">
              <div className="col-12 h-100 position-relative pl-0">
                <div className="image-container">
                  <div className="hover"></div>
                  <img src="/images/news/wholesale.jpg" />
                </div>
                <div className="contents py-5 fg-black position-relative">
                  <div
                    className="industry bg-green font-bold fg-white"
                    title="Water"
                  >
                    W
                  </div>
                  <h4 className="mt-5 col-12 font-medium col-4 pl-0">
                    Trade agents and wholesalers back on track with e-commerse
                    trading
                  </h4>
                  <p>
                    TAP solution we introduced to the users to get them
                    experience advanced ecommerse platform for trade agents and
                    wholesalers
                  </p>
                  <Link className="mt-4 fg-white col-12 p-0" to="#">
                    Read More
                  </Link>
                </div>
              </div>
            </div>
            <div className="box-half col-12 col-md-6 p-0">
              <div className="col-12 h-100 position-relative pl-0">
                <div className="image-container">
                  <div className="hover"></div>
                  <img src="/images/news/farming-drone.jpg" />
                </div>
                <div className="contents py-5 fg-black position-relative">
                  <div
                    className="industry bg-blue font-bold fg-white"
                    title="Water"
                  >
                    A
                  </div>
                  <h4 className="mt-5 col-12 font-medium col-4 pl-0">
                    Farming drone
                  </h4>
                  <p>
                    Agritech: We aspire to become an inspiration for the next
                    generations through the projects we deliver.
                  </p>
                  <Link className="mt-4 fg-white col-12 p-0" to="#">
                    Read More
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
