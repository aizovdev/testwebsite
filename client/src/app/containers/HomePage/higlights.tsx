import React, { Component } from 'react';
import Carousel from 'react-multi-carousel';
import { Link } from 'react-router-dom';
import axios from 'axios';
import 'react-multi-carousel/lib/styles.css';
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 9000, min: 1600 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 1600, min: 1024 },
    items: 4,
  },
  tablet: {
    breakpoint: { max: 1024, min: 550 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 550, min: 0 },
    items: 1,
  },
};
export default class higlights extends Component<{}, any> {
  constructor(props) {
    super(props);
    this.state = {
      blogs: '',
    };
  }
  componentDidMount() {
    let route = 'http://127.0.0.1:8000/api/newsexternalapi';
    axios.get(route).then(response => {
      console.log(response.data.status);
      if (response.data.status == 101) {
        console.log(response.data.data);
        this.setState({ blogs: response.data.data });
      }
    });
  }
  render() {
    return (
      <div className="home-top-banner">
        <div className="container">
          <div className="row highlight-slide no-pr-xs">
            <Carousel
              className="h-50 "
              responsive={responsive}
              additionalTransfrom={0}
              arrows
              autoPlaySpeed={3000}
              centerMode={true}
              containerClass="container-fluid"
              draggable
              focusOnSelect={false}
              infinite={false}
              itemClass=""
              keyBoardControl
              minimumTouchDrag={80}
              renderButtonGroupOutside={false}
              renderDotsOutside={false}
            >
              {this.state.blogs &&
                this.state.blogs.map((item, index) => {
                  return (
                    <div className="h-100">
                      <Link
                        className="slider-box"
                        to={`/blogs/${item.id}/${item.title}`}
                      >
                        <div className="col-12 h-100">
                          <div className="slider-images animate bg-green rounded overflow-hidden pb-0">
                            <div className="image h-100">
                              <div className="row h-100 position-relative">
                                <img
                                  className="absolute-bottom cover-img"
                                  src={`http://127.0.0.1:8000/api/v1/file?d=news&name=news/${item.background_image}`}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="details pt-4">
                            <span>
                              {item.tag &&
                                item.tag.map((tag, indexex) => {
                                  return (
                                    <span>
                                      {tag.name} {tag}
                                    </span>
                                  );
                                })}{' '}
                            </span>
                            <h3 className="font-avg font-medium fg-black font-df pt-2">
                              {item.title}
                            </h3>
                            <p className="pt-1 fg-dark">
                              {item.description
                                .substring(0, 100)
                                .replace(/(<([^>]+)>)/gi, '')}
                            </p>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                })}
            </Carousel>
          </div>
        </div>
      </div>
    );
  }
}
