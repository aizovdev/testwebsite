import React from 'react';
import { Helmet } from 'react-helmet-async';
import research from './assets/images/research.jpg';
import innovate from './assets/images/innovate.jpg';
import inspire from './assets/images/inspire.jpg';
import Higlights from './higlights';
import Solution from './solution';
import Research from './research';
export function HomePage() {
  return (
    <>
      <Helmet>
        <meta
          property="og:title"
          content="We are determined to bring a global community that strives towards more sustainable living for today and for generations to come"
        ></meta>
        <meta property="og:type" content="product"></meta>
        <meta property="og:url" content="https://www.aizove.com"></meta>
        <meta property="og:image:width" content="800"></meta>
        <meta property="og:image:height" content="530"></meta>
        <meta property="og:site_name" content="Aizove"></meta>
        <meta name="twitter:card" content="summary"></meta>
        <meta name="twitter:site" content="@aizove"></meta>
        <meta name="twitter:domain" content="aizove"></meta>
        <meta name="twitter:title" content="Research Innovate Inspire"></meta>

        <title>Aizove - Research Innovate and Inspire</title>

        <meta
          name="description"
          content="We are determined to bring a global community that strives towards more sustainable living for today and for generations to come"
        />

        <meta
          name="keywords"
          content="Our research continues to develop and enhance current Technology, Education, Healthcare, Community, Energy and Biodiversity."
        />

        <link rel="canonical" href="#" />
        <meta property="og:title" content="Research Innovate and Inspire" />
        <meta property="og:type" content="product" />
        <meta property="og:url" content="#" />
        <meta
          property="og:image"
          content="https://www.aizove.com/images/news/team.jpg"
        />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="628" />
        <meta property="og:site_name" content="Aizove" />
        <meta
          property="og:description"
          content="Our research continues to develop and enhance current Technology, Education, Healthcare, Community, Energy and Biodiversity"
        />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@Aizove" />
        <meta name="twitter:domain" content="Aizove" />
        <meta
          name="twitter:title"
          content="Aizove - Research Innovate Inspire"
        />
        <meta
          name="twitter:description"
          content="Our research continues to develop and enhance current Technology, Education, Healthcare, Community, Energy and Biodiversity"
        />
        <meta
          name="twitter:image"
          content="https://www.aizove.com/images/news/team.jpg"
        />
      </Helmet>

      {/* Start top Image Banner */}
      <div className="container py-5">
        <h1 className="font-big l-15 py-5 px-0 col-md-10">
          We are <span className="font-bold">determined to bring</span> a global
          community that strives towards
          <span className="font-bold fg-green"> more sustainable </span>living
          for today and for
          <span className="font-bold"> generations</span> to come…
        </h1>
      </div>
      {/* end image banner */}
      {/* Start top Image Banner */}
      <div className="container pb-5">
        <div className="awesome">
          <div className="row">
            <div className="col-lg-4 mb-4 col-md-4 col-sm-6 col-12">
              <div className="container-foto col-12 p-0">
                <div className="ver-mas fg-white bg-green p-3">
                  <h3 className="font-big font-black">Research</h3>
                  <p>
                    Our research continues to develop and enhance current
                    Technology, Education, Healthcare, Community, Energy and
                    Biodiversity.
                  </p>
                </div>

                <img src={research} alt="" />
              </div>
            </div>

            <div className="col-lg-4 mb-4 col-md-4 col-sm-6 col-12">
              <div className="container-foto col-12 p-0">
                <div className="ver-mas fg-white bg-purple p-3">
                  <h3 className="font-big font-black">Innovate</h3>
                  <p>
                    Our innovation is at the aim to deliver better solutions
                    that meet new requirements and existing market needs.
                  </p>
                </div>

                <img src={innovate} alt="" />
              </div>
            </div>

            <div className="col-lg-4 mb-4 col-md-4 col-sm-12 col-12">
              <div className="container-foto col-12 p-0">
                <div className="ver-mas fg-white bg-yellow p-3">
                  <h3 className="font-big font-black">Inspire</h3>
                  <p>
                    We aspire to become an inspiration for the next generations
                    through the projects we deliver.
                  </p>
                </div>

                <img src={inspire} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* end image banner */}
      <Higlights></Higlights>
      <div id="research">
        <Solution></Solution>
      </div>
      <div id="solutions">
        <Research></Research>
      </div>
    </>
  );
}
