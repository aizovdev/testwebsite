import { HomePage } from './containers/HomePage/Loadable';
import { Login, Signup, Otpverification } from './containers/Auth/Loadable';

import Auth from './containers/Auth/Loadable';
const route = [
  {
    path: '/home',
    name: 'dewa',
    icon: 'tim-icons icon-chart-pie-36',
    component: HomePage,
    layout: 'dewa',
  },
  {
    path: '/auth/login',
    name: 'dewa',
    icon: 'tim-icons icon-chart-pie-36',
    component: Login,
    layout: 'dewa',
  },
  {
    path: '/auth/signup',
    name: 'dewa',
    icon: 'tim-icons icon-chart-pie-36',
    component: Signup,
    layout: 'dewa',
  },
  {
    path: '/auth/otp',
    name: 'dewa',
    icon: 'tim-icons icon-chart-pie-36',
    component: Otpverification,
    layout: 'dewa',
  },
  {
    path: '/',
    name: 'dewa',
    icon: 'tim-icons icon-chart-pie-36',
    component: HomePage,
    layout: 'dewa',
  },
];
export default route;
