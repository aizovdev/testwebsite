import React from 'react';
import { FaSpinner } from 'react-icons/fa';

export default function spinner() {
  return (
    <div className="ml-2 float-right">
      <FaSpinner />
    </div>
  );
}
