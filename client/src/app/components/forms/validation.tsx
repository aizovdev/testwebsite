const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
);

export function Validation(data) {
  const { name, value } = data.target;
  let label: any = data.target.getAttribute('data-label');

  /** minimum character validation  */
  if (
    data.target.getAttribute('data-min') &&
    value.length < data.target.getAttribute('data-min')
  ) {
    let result: any = {
      message:
        label +
        ' must be at least ' +
        data.target.getAttribute('data-min') +
        ' character long !',
      field: name,
    };
    return result;
  }
  /** maximum character validation  */
  if (
    data.target.getAttribute('data-max') &&
    value.length > data.target.getAttribute('data-max')
  ) {
    let result: any = {
      message:
        'Maximum allowed characters are ' +
        data.target.getAttribute('data-max'),
      field: name,
    };
    return result;
  }
  /** email validation  */
  if (data.target.getAttribute('data-email') && !validEmailRegex.test(value)) {
    let result: any = { message: label + ' is not valid !', field: name };
    return result;
  }
  /** required validation */
  if (data.target.getAttribute('data-required') && value.length <= 0) {
    let result: any = { message: label + ' is required !', field: name };
    return result;
  }
  if (data.target.getAttribute('data-password')) {
    if (value.search(/[a-z]/i) < 0) {
      let result: any = {
        message: 'Your password must contain at least one letter.',
        field: name,
      };
      return result;
    }
    if (value.search(/[0-9]/) < 0) {
      let result: any = {
        message: 'Your password must contain at least one digit.',
        field: name,
      };
      return result;
    }
  }

  let result: any = { message: '', field: name };
  return result;
}

export function IsValid(obj) {
  return !Object.values(obj).some(element => element !== '');
}
