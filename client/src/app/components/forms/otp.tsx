import React, { Component } from 'react';
import { Validation, IsValid } from './validation';
import axios from 'axios';
import { Link } from 'react-router-dom';
import {
  GoogleReCaptchaProvider,
  GoogleReCaptcha,
} from 'react-google-recaptcha-v3';

const API_PATH = process.env.REACT_APP_API_PATH + '/api/users/signin';
const RECAPTCHA_SITE_KEY = process.env.REACT_APP_RECAPTCHA_SITE_KEY;

export default class otp extends Component<{}, any> {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      otp: null,
      errors: {
        otp: '',
      },
    };
  }
  handleChange = event => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;
    this.setState({ name: value });
    let validation = Validation(event);
    if (validation) {
      errors[validation.field] = validation.message;
    }

    this.setState({ errors, [name]: value });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.setState({ mailSent: false });
    this.setState({ error: null });
    if (IsValid(this.state.errors)) {
      axios({
        method: 'post',
        url: `${API_PATH}`,
        headers: {
          'content-type': 'application/json',
        },
        data: this.state,
      })
        .then(result => {
          if (result.status == 200) {
            this.setState({
              success: 'Sign in success.',
            });
          } else {
            this.setState({ error: result.data.message });
          }
        })

        .catch(({ response }) =>
          response && Object.keys(response.data).length
            ? Object.keys(response.data).forEach(key => {
                let errors = this.state.errors;
                errors[key] = response.data[key];
                this.setState({ errors, [key]: response.data[key] });
              })
            : this.setState({ error: 'Network error !' }),
        );
    } else {
      console.error('Invalid Form');
    }
  };
  handleVerify() {}
  render() {
    return (
      <div className="p-5 bordered aizove-form d-inline-flex text-left">
        <form
          method="POST"
          onSubmit={this.handleSubmit}
          action={API_PATH}
          className="col-12"
        >
          <h4 className="py-4">Continue with your Aizove account</h4>
          {this.state.success && (
            <div className="btn-success col-12">{this.state.success}</div>
          )}
          <div className="input-group">
            <label>Otp</label>
            <input
              type="text"
              name="otp"
              onChange={this.handleChange}
              required
              data-required="true"
              data-label="Otp"
            />
            <span className="danger col-12 p-0 fg-red">
              {this.state.errors.otp}
            </span>
          </div>

          <button className="button-st-1  btn float-right bg-blue fg-white mt-3">
            Submit
          </button>
          {this.state.error && (
            <div className="danger col-12 p-0 fg-red">{this.state.error}</div>
          )}
          <GoogleReCaptchaProvider reCaptchaKey={RECAPTCHA_SITE_KEY}>
            <GoogleReCaptcha onVerify={this.handleVerify} />
          </GoogleReCaptchaProvider>
        </form>
      </div>
    );
  }
}
