import React, { Component } from 'react';
import axios from 'axios';
import { Validation, IsValid } from './validation';
import { Link, Redirect } from 'react-router-dom';

import {
  GoogleReCaptchaProvider,
  GoogleReCaptcha,
} from 'react-google-recaptcha-v3';
const API_PATH = process.env.REACT_APP_API_PATH + '/api/users/signin';
const RECAPTCHA_SITE_KEY = process.env.REACT_APP_RECAPTCHA_SITE_KEY;

export default class signin extends Component<{}, any> {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      userName: null,
      password: null,
      errors: {
        userName: '',
        password: '',
      },
    };
  }
  handleChange = event => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;
    this.setState({ name: value });
    let validation = Validation(event);
    if (validation) {
      errors[validation.field] = validation.message;
    }

    this.setState({ errors, [name]: value });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.setState({ mailSent: false });
    this.setState({ error: null });
    // if (IsValid(this.state.errors)) {
    axios({
      method: 'post',
      url: `${API_PATH}`,
      headers: {
        'content-type': 'application/json',
      },
      data: this.state,
    })
      .then(result => {
        if (result.status == 200) {
          let obj = Object.assign({}, this.state);
          obj.errors['userName'] = '';
          obj.errors['password'] = '';
          this.setState(obj);
          this.setState({
            success: 'Sign in success.',
          });
          return (window.location.href = '/home');
        } else {
          this.setState({ error: result.data.message });
        }
      })

      .catch(({ response }) =>
        response && Object.keys(response.data).length
          ? Object.keys(response.data).forEach(key => {
              let obj = Object.assign({}, this.state);
              obj.errors[key] = response.data[key];
              this.setState(obj);
            })
          : this.setState({ error: 'Network error !' }),
      );
  };
  handleVerify() {}
  render() {
    return (
      <div className="p-5-md bordered aizove-form d-inline-flex text-left">
        <form
          method="POST"
          onSubmit={this.handleSubmit}
          action={API_PATH}
          className="col-12"
        >
          <h4 className="py-4">Continue with your Aizove account</h4>
          {this.state.success && (
            <div className="btn-success col-12">{this.state.success}</div>
          )}

          <div className="input-group">
            <label>Username</label>
            <input
              type="text"
              name="userName"
              placeholder="Username/Email"
              onChange={this.handleChange}
              required
              data-required="true"
              data-label="User Name"
            />
            <span className="danger col-12 p-0 fg-red">
              {this.state.errors.userName}
            </span>
          </div>
          <div className="input-group pt-3">
            <label>Password</label>
            <input
              type="password"
              onChange={this.handleChange}
              required
              placeholder="password"
              name="password"
              data-required="true"
              data-label="Password"
            />
            <span className="danger col-12 p-0 fg-red">
              {this.state.errors.password}
            </span>
          </div>

          <GoogleReCaptchaProvider reCaptchaKey={RECAPTCHA_SITE_KEY}>
            <GoogleReCaptcha onVerify={this.handleVerify} />
          </GoogleReCaptchaProvider>
          <div className="button-lineheight">
            Create new account? <Link to="/auth/signup">Signup</Link>{' '}
            <button className="button-st-1  btn float-right bg-blue fg-white">
              Submit
            </button>
          </div>
          {this.state.error && (
            <div className="danger col-12 p-0 fg-red">{this.state.error}</div>
          )}
        </form>
      </div>
    );
  }
}
