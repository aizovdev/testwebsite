import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Validation, IsValid } from './validation';
import axios from 'axios';
import Spinner from '../utils/spinner';
import {
  GoogleReCaptchaProvider,
  GoogleReCaptcha,
} from 'react-google-recaptcha-v3';

const validateForm = errors => {
  let valid = true;
  //   Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};
const RECAPTCHA_SITE_KEY = process.env.REACT_APP_RECAPTCHA_SITE_KEY;
const API_PATH = process.env.REACT_APP_API_PATH + '/api/users/signup';
export default class signup extends Component<{}, any> {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      firstName: null,
      lastName: null,
      email: null,
      mobile: null,
      userName: null,
      password: null,
      confirmPassword: null,
      error: null,
      success: null,
      loading: false,
      errors: {
        firstName: '',
        lastName: '',
        email: '',
        mobile: '',
        userName: '',
        password: '',
      },
    };
  }

  handleChange = event => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;
    this.setState({ name: value });
    let validation = Validation(event);
    if (validation) {
      errors[validation.field] = validation.message;
    }
    switch (name) {
      case 'confirmPassword':
        if (this.state.password != value) {
          errors.password = 'Passwords are not equal!';
        } else {
          errors.password = '';
        }
        break;
      default:
        break;
    }

    this.setState({ errors, [name]: value });
  };

  handleSubmit = event => {
    event.preventDefault();
    let errors = this.state.errors;

    this.setState({ mailSent: false });
    this.setState({ error: null });
    if (IsValid(this.state.errors)) {
      this.setState({ loading: true });
      axios({
        method: 'post',
        url: `${API_PATH}`,
        headers: {
          'content-type': 'application/json',
        },
        data: this.state,
      })
        .then(result => {
          this.setState({ loading: false });
          if (result.status == 200) {
            this.setState({
              success: 'Your account has been created successfully',
            });
            return (window.location.href = '/home');
          } else {
            this.setState({ error: result.data.message });
          }
        })

        .catch(({ response }) =>
          response.data
            ? Object.keys(response.data).forEach(key => {
                this.setState({ loading: false });

                let obj = Object.assign({}, this.state);
                obj.errors[key] = response.data[key];
                this.setState(obj);
              })
            : this.setState({ loading: false }),
        );
    } else {
      console.error('Invalid Form');
    }
  };
  handleVerify() {}
  render() {
    const { errors } = this.state;
    return (
      <>
        <div className="p-5-md bordered aizove-form col-lg-7 col-11 col-md-8 mx-auto text-left">
          <form method="post" onSubmit={this.handleSubmit} action={API_PATH}>
            <div className="row">
              <h4 className="col-12 my-4">Create your Aizove account</h4>
              {this.state.success && (
                <div className="btn-success col-12">{this.state.success}</div>
              )}
              <div className="col-md-6 col-12 pb-3">
                <label>First Name</label>
                <input
                  type="text"
                  onChange={this.handleChange}
                  name="firstName"
                  placeholder="First Name"
                  required
                  data-min="3"
                  data-label="First Name"
                />
                <span className="danger col-12 p-0 fg-red">
                  {this.state.errors.firstName}
                </span>
              </div>
              <div className="col-md-6 col-12 pb-3">
                <label>Last Name</label>
                <input
                  type="text"
                  onChange={this.handleChange}
                  name="lastName"
                  placeholder="Last Name"
                  required
                  data-min="3"
                  data-label="Last Name"
                />
                <span className="danger col-12 p-0 fg-red">
                  {this.state.errors.lastName}
                </span>
              </div>
              <div className="col-md-6 col-12 pb-3">
                <label>Email Address</label>
                <input
                  type="text"
                  onChange={this.handleChange}
                  name="email"
                  placeholder="Email"
                  required
                  data-email="true"
                  data-label="Email"
                />
                <span className="danger col-12 p-0 fg-red">
                  {this.state.errors.email}
                </span>
              </div>
              <div className="col-md-6 col-12 pb-3">
                <label>Mobile</label>
                <input
                  type="text"
                  onChange={this.handleChange}
                  name="mobile"
                  required
                  placeholder="Mobile Number with Country Codes"
                  data-required="true"
                  data-label="Mobile number"
                />
                <span className="danger col-12 p-0 fg-red">
                  {this.state.errors.mobile}
                </span>
              </div>
              <div className="col-12 pb-3">
                <label>Username</label>
                <input
                  type="text"
                  onChange={this.handleChange}
                  name="userName"
                  required
                  placeholder="User Name"
                  data-required="true"
                  data-min="5"
                  data-label="User Name"
                />
                <span className="danger col-12 p-0 fg-red">
                  {this.state.errors.userName}
                </span>
              </div>
              <div className="col-12 col-md-6 pb-3">
                <label>Password</label>
                <input
                  type="password"
                  onChange={this.handleChange}
                  name="password"
                  required
                  data-required="true"
                  placeholder="Password"
                  data-password="true"
                  data-min="8"
                  data-max="12"
                  data-label="Password"
                />
                <span className="danger col-12 p-0 fg-red">
                  {this.state.errors.password}
                </span>
              </div>
              <div className="col-md-6 col-12 pb-3">
                <label>Confirm Password</label>
                <input
                  type="password"
                  onChange={this.handleChange}
                  name="confirmPassword"
                  placeholder="Password"
                  required
                />
              </div>
              <div className="button-lineheight col-12">
                Already have account ? <Link to="/auth/login">Login</Link>{' '}
                <button className="button-st-1  btn float-right bg-blue fg-white">
                  Next {this.state.loading ? <Spinner /> : ''}
                </button>
              </div>
            </div>
            <GoogleReCaptchaProvider reCaptchaKey={RECAPTCHA_SITE_KEY}>
              <GoogleReCaptcha onVerify={this.handleVerify} />
            </GoogleReCaptchaProvider>
          </form>
          <div className="col-sm-12 p-0 text-center pt-4">
            {this.state.mailSent && (
              <div className="success">Thank you for contacting us.</div>
            )}

            {this.state.error && (
              <div className="danger col-12 p-0 fg-red">{this.state.error}</div>
            )}
          </div>
        </div>
      </>
    );
  }
}
