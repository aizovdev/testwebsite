/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */
// These must be the first lines in src/index.js

import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React, { Component } from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import { GlobalStyle } from 'styles/global-styles';
import "./assets/scss/app.scss";
// import { HomePage } from './containers/HomePage/Loadable';
import { NotFoundPage } from './components/NotFoundPage/Loadable';
import { Header, Footer } from './containers/Layout/Loadable';
import routes from "./routes";
class App extends Component {
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "dewa") {
        return (
          <Route onUpdate={() => window.scrollTo(0, 0)}
            path={prop.path}
            component={prop.component}
          />
        );
      } else {
        return null;
      }
    });
  };
  render() {
    return (
      <BrowserRouter>
        <Helmet
          titleTemplate="%s - React Boilerplate"
          defaultTitle="React Boilerplate"
        >
          <meta name="description" content="A React Boilerplate application" />
        </Helmet>
        <Header />
        <Switch>
          {this.getRoutes(routes)}
          <Route component={NotFoundPage} />
        </Switch>
        <Footer />
        <GlobalStyle />
      </BrowserRouter>
    );
  }
}
export default App;