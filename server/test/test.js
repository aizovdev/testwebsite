const assert = require('assert');
const request = require('supertest');
const express = require('express');
const app = express();

// DOCS : https://mochajs.org/#getting-started

describe('Array', () => {
  describe('#indexOf()', () => {
    it('should return -1 when the value is not present', () => {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});

describe('POST /login', function () {
  it('responds with json', function (done) {
    request(app)
      .post('api/users/signin')
      .send({
        email: "iqbalukkadan@gmail.com",
        password: "iqbaluk123",
      })
      .set('Accept', 'application/json')
      // .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});