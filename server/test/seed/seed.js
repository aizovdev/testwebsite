const {ObjectID} = require('sequelize');
const jwt = require('jsonwebtoken');
const {User} = require('./../../models/user');

const userOneID = 11;
const userTwoID = 12;
const users = [{
  _id: userOneID,
  email: "userone@gmail.com",
  password: "useronepassword",
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userOneID, access: 'auth'}, "process.env.JWT_SECRET").toString()
  }]
}, {
  _id: userTwoID,
  email: "usertwo@gmail.com",
  password: "usertwopassword",
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userTwoID, access: 'auth'}, "process.env.JWT_SECRET").toString()
  }]
}]


var populateUsers = (done) => {
  User.remove({}).then(() => {
    var userOne = new User(users[0]).save();
    var userTwo = new User(users[1]).save();

    return Promise.all([userOne, userTwo])
  }).then(() => done());
};

module.exports = {
  users,
  populateUsers
}