'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Please enter your first name'
        }
      }
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Please enter your last name'
        }
      }
    },
    mobile: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Please enter contact number'
        }
      }
    },
    email: {
      type: DataTypes.STRING, validate: {
        isUnique: function (value, next) {
          var self = this;
          User.findOne({ where: { email: value } })
            .then(function (user) {
              // reject if a different user wants to use the same email
              if (user && self.id !== user.id) {
                throw new Error("Email already in use!");
              }
              return next();
            })
            .catch(function (err) {
              return next(err);
            });
        }
      }
    },
    userName: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'Please enter user name'
        },
        isUnique: function (value, next) {
          var self = this;
          User.findOne({ where: { userName: value } })
            .then(function (user) {
              // reject if a different user wants to use the same email
              if (user && self.id !== user.id) {
                throw new Error("User name already in use!");
              }
              return next();
            })
            .catch(function (err) {
              return next(err);
            });
        }
      }
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Please enter password'
        }
      }
    },
    
  }, {
    sequelize,
    modelName: 'User',
  });

  return User;
};